import { Component, OnInit } from '@angular/core';
import { ProductModel } from '../model/model';
import { Service } from '../service/service';
import {FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'product',
  templateUrl: './product.component.html',

})
export class ProductComponent implements OnInit {

  // Variable Declaration
  filterForm!: FormGroup;
  loading: boolean = false;
  productList: ProductModel[] = [];
  filteredProductList: ProductModel[] = [];
  category: string = '';


  // Constructor
  constructor(
    private _service: Service,
  ) {

    // Formgroup
    this.filterForm = new FormGroup({
      minPrice: new FormControl(),
      maxPrice: new FormControl(),
      minDiscount: new FormControl(),
      maxDiscount: new FormControl()
    });

    // Acessing value on
    this.filterForm?.valueChanges.subscribe(value => {
      this.filterProductByPriceAndDiscount(value);
    });
  }


  // Ng Onit
  ngOnInit() {
    this.getList();
  }

  // Fetching Product List from API and Storing in Array
  getList() {
    this._service.list().subscribe((results) => {
      this.productList = results;
      this.filteredProductList = this.productList;
    });
  }

  // For filtering productList by category
  filterProductByCategory(item: string) {
    this.category = item;
    if (this.category != '') {
      this.filteredProductList = this.productList.filter(element => element.category == item)
    } else
      this.filteredProductList = this.productList;
  }

  // For filtering productlist by price and discount
  filterProductByPriceAndDiscount(_value: any) {
    const minPrice = this.filterForm.value.minPrice;
    const maxPrice = this.filterForm.value.maxPrice;
    const minDiscount = this.filterForm.value.minDiscount;
    const maxDiscount = this.filterForm.value.maxDiscount;

    if (minPrice != null && maxPrice != null && minDiscount == null && maxDiscount == null) {
      this.filteredProductList = this.productList.filter(element => element.price >= minPrice && element.price <= maxPrice)
    } else if (minPrice == null && maxPrice == null && minDiscount != null && maxDiscount != null) {
      this.filteredProductList = this.productList.filter(element => element.discount >= minDiscount && element.discount <= maxDiscount)
    } else if (minPrice !== null && maxPrice !== null && minDiscount != null && maxDiscount != null) {
      this.filteredProductList = this.productList.filter(element => element.price >= minPrice && element.price <= maxPrice && element.discount >= minDiscount && element.discount <= maxDiscount)
    } else
      this.filteredProductList = this.productList;
  }

  // For clearing filter
  clearFiltering() {
    this.category = '';
    this.filterForm.reset();
  }
}