export interface ProductModel {
    id: number;
    name: string;
    price: number;
    discount: number;
    image: ImageData;
    category:string;
}