import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class Service {
    private url: string = 'https://6423e3dbd6152a4d48018ff9.mockapi.io/api/product';
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    // Constructor
    constructor(private http: HttpClient) {}

    // public handleError(operation = 'operation') {
    //     return (error: any) => {
    //         // alert(`${operation} failed ${error.error.message}`);
    //         // this.notifyService.show(`${operation} failed: ${error.error.message}`);


    //         // return throwError(() => error.error.message);
    //     }
    // }

    // For Fetching Product List
    list(): Observable<any> {
        return this.http
            .get<any>(
                this.url, this.httpOptions
            )
        // .pipe(catchError(this.baseService.handleError('Get User')));
    }

   
}
