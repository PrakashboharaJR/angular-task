import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Categories'
})
export class CategoriesPipe implements PipeTransform {
  transform(products: any[]): string[] {
    const categories = products.map(p => p.category);
    return categories.filter((c, index) => categories.indexOf(c) === index);
  }
}
