import { Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';

const appRoutes: Routes = [
  { path: '', 
    component: ProductComponent 
  }
];
export default appRoutes;